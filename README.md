# Games
A small collection of games written in various languages. The games were written solely as a personal challenge to keep my skills sharp.

### Guess.pl
A simple guessing game written in Perl.
Usage: `perl guess.pl`.
