#/usr/bin/perl
use strict;
use warnings;
use feature qw(switch);
# set to zero to supress output about the game's mechanics
my $verbose = 1;

#set to zero to disable debug messages
my $debug = 0;

# greet the player
print "Welcome to the guessing game!\n";
print "The following difficulties are available:\n1. Easy\n2. Normal\n3. Hard\n4. Insane\n";
print "Please select your desired difficulty level [1-4]: ";

# configure difficulty settings
my $difficulty = <>;
chomp($difficulty);

my $max;
print "Difficulty $difficulty selected.\n" if $verbose==1;


given ($difficulty)
{
	when(1)	{$max=3;}
	when(2) {$max=5;}
	when(3) {$max=7;}
	when(4) {$max=10;}
	default {die "Invalid difficulty selected!\n";}
}
print "You will be asked to pick a number between 1 and $max\n";

my $answer = int(rand($max));
print "Answer is $answer.\n" if $debug==1;

print "Enter your guess: ";
my $guess = <>;
chomp($guess);

if ($guess == $answer)
{
	print "You won! Your guess of $guess was correct.\n";
	exit 0;
}
else
{
	print "Better luck next time. You guessed $guess when the answer was $answer.\n";
	exit 0;
}
